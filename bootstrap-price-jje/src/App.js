import { Container} from "react-bootstrap";
import AppRoute from "./MainRoute";

function App() {
  return (
    <Container className="p-3">
      <AppRoute/>
    </Container>
  );
}

export default App;
