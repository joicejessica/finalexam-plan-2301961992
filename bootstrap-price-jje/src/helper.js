import { FiAnchor, FiGitlab, FiAward } from "react-icons/fi"

export const getIconComponentByName = (icon, size, color) => {
    switch (icon) {
        case "pirate":
            return <FiAnchor size={size} color={color} />
        case "catman":
            return <FiGitlab size={size} color={color} />
        case "champ":
            return <FiAward size={size} color={color} />
        default:
            return <FiAnchor size={size} color={color} />;
    }
}