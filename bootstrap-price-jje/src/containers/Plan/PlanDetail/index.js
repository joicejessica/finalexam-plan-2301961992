import React from 'react'
import { Col, Row, Stack } from 'react-bootstrap';
import { useLocation, useNavigate } from 'react-router-dom';
import { FiArrowLeftCircle } from "react-icons/fi"
import { getIconComponentByName } from '../../../helper';
import { COLOR } from '../../../constant';

const PlanDetail = (props) => {
    const location = useLocation()
    const navigate = useNavigate();
    const { title, benefits, price, priceInfo, icon, color } = location?.state

    const backHandler = () => {
        navigate('/');
    }

    return (
        <>
            <Row>
                <Col className='text-white  lh-lg'>
                    <Stack direction='horizontal'  className="mb-4">
                        <button className='button-back' onClick={backHandler}>
                            <FiArrowLeftCircle size={32} color={color} />
                        </button>
                        <h1 className='ms-auto me-auto fw-bold mb-0' style={{ color }}>{title}</h1>
                        {getIconComponentByName(icon, 40, color)}
                    </Stack>
                    <hr />
                    <p> Sed porta molestie volutpat. In malesuada ultricies posuere. Proin tincidunt magna scelerisque, efficitur enim vel, faucibus felis. Nulla ornare pharetra lectus, in placerat orci consectetur sit amet. Fusce auctor nisl fringilla augue pellentesque, ut convallis metus elementum. Sed vestibulum mi finibus justo semper, nec commodo turpis ultricies. Phasellus tristique rhoncus felis, vitae mollis lorem venenatis in. Maecenas quis nisl sed sem accumsan egestas.
                    </p>
                    <p> Duis mollis eu nibh et egestas. Sed lobortis, ex fringilla sollicitudin interdum, sapien massa auctor ligula, et sagittis lorem felis at lacus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque lacinia cursus sapien eu laoreet. Sed vel gravida neque. Pellentesque aliquet a felis at mattis. Quisque id lobortis erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Duis in nunc ipsum. Etiam euismod commodo dolor ut venenatis. Fusce et vulputate elit, dignissim posuere urna. Praesent quam dolor, ullamcorper nec maximus eu, semper ac augue. Proin maximus tincidunt nisi id lacinia.</p>
                    <hr />
                </Col>
            </Row>
            <Row>
                <Col md={6}>
                    <ul className='font-secondary lh-lg text-white  lh-lg'>
                        {benefits.map((benefit, index) => <li key={`detail-${index}`}>{benefit}</li>)}
                    </ul>
                </Col>
                <Col md={6}>
                    <Stack className=' fw-bold'>
                        <span style={{ color }} className="display-6 mb-0 fw-bold ms-auto mt-4 detail-price">IDR {price}</span>
                        <span className='ms-auto detail-price-info' style={{ color : COLOR.NEUTRAL_COLOR }}>{priceInfo}</span>
                    </Stack>
                </Col>
            </Row>

        </>
    )
}

export default PlanDetail