import React from 'react'
import { Col, Row } from 'react-bootstrap';
import PlanCard from './partials/PlanCard';
import { COLOR } from '../../../constant';

const plans = [
    {
      id: 1,
      icon: 'pirate',
      title: 'PIRATE PLAN',
      benefits: ['Lifetime update', 'Daily rewards', 'Annually report'],
      price: "950.000",
      priceInfo: 'per month',
      color: COLOR.PRIMARY_COLOR,
      isActive: false,
    },
    {
      id: 2,
      icon: 'catman',
      title: 'CATMAN PLAN',
      benefits: ['Lifetime update', 'Daily rewards', 'Annually report'],
      price: "1.590.000",
      priceInfo: 'per month',
      color: COLOR.SECONDARY_COLOR,
      isActive: true,
    },
    {
      id: 3,
      icon: 'champ',
      title: 'CHAMP PLAN',
      benefits: ['Lifetime update', 'Daily rewards', 'Annually report'],
      price: "4.590.000",
      priceInfo: 'per month',
      color: COLOR.TERTIARY_COLOR,
      isActive: false,
    }
  ]

const Plans = () => {
  return (
    <Row>
        {
          plans.map((plan, index) => <Col key={`plan-${index}}`} md={6} lg={4}>
            <PlanCard  {...plan} />
          </Col>)
        }
      </Row>
  )
}

export default Plans