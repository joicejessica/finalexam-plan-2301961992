import React from 'react'
import PropTypes from "prop-types"
import { Card, ListGroup } from 'react-bootstrap'
import cx from 'classnames'
import { COLOR } from '../../../../constant'
import { Link } from 'react-router-dom'
import { getIconComponentByName } from '../../../../helper'

const PlanCard = (props) => {
  const {id, icon, title, benefits, price, priceInfo, color, isActive} = props
  return (
    <Link  to={{ pathname: `/detail`}} state={{ title, benefits, color, price, priceInfo, icon }}>
      <Card className={cx('border border-light p-4  text-center mb-4', { 'bg-transparent': !isActive  }, { 'bg-white': isActive })}>
      <Card.Body>
        <div className='mb-4'>{getIconComponentByName(icon, 80, color)}</div>
        <Card.Title className="fs-3 mb-3 fw-bolder" style={{ color: isActive ? COLOR.DARK_COLOR : COLOR.LIGHT_COLOR }}>{title}</Card.Title>
        <ListGroup  variant='flush' className='border-transparent py-4 border-top border-bottom mb-3 font-secondary'>
        { benefits?.map((benefit, index) => <ListGroup.Item key={`plan-listgroup-${index}${id}`} className='bg-transparent border-0' style={{ color: isActive ? COLOR.NEUTRAL_COLOR : COLOR.LIGHT_COLOR }} >{benefit}</ListGroup.Item>)}
        </ListGroup>
        <Card.Text style={{color}} className="mb-0 fs-4 fw-bold font-primary">{`IDR ${price}`}</Card.Text>
        <Card.Text className='fw-semibold font-primary' style={{ color: isActive ? COLOR.NEUTRAL_COLOR2 : COLOR.LIGHT_COLOR }} >{priceInfo}</Card.Text>
      </Card.Body>
    </Card>
    </Link>
  )
}

PlanCard.propTypes = {
  id: PropTypes.number.isRequired,
    icon: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    benefits: PropTypes.arrayOf(PropTypes.string).isRequired,
    price: PropTypes.string.isRequired,
    priceInfo: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    isActive: PropTypes.bool.isRequired,
}

export default PlanCard