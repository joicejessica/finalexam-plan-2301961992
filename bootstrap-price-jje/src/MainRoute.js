import React from 'react'
import {
    BrowserRouter as Router,
    Routes,
    Route,
  } from "react-router-dom";
import PlanDetail from './containers/Plan/PlanDetail';
import Plans from './containers/Plan/Plans';

const AppRoute = () => {
  return (
    <Router>
        <Routes>
            <Route path='/' element={<Plans/>} />
            <Route path="/detail" element={<PlanDetail/>}/>
        </Routes>
    </Router>
  )
}

export default AppRoute